#!/usr/bin/env gjs

const Lang = imports.lang;
const Mainloop = imports.mainloop;
const System = imports.system;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;

const DBusChat = {};
(function(namespace_in) {
    let namespace = namespace_in || {};
    const CALLTIMEOUT = 5;
    let CreateDBusObject = function(bustype,dest,path,iface) {
	let conn = Gio.bus_get_sync(bustype, null);
	let ispect_iface = "org.freedesktop.DBus.Introspectable";
	let ispect_method = "Introspect";
	
	let xml = conn.call_sync(dest,
				 path,
				 ispect_iface,
				 ispect_method,
				 null,
				 new GLib.VariantType("(s)"),
				 Gio.DBusCallFlags.NONE,
				 CALLTIMEOUT,
				 null).unpack()[0].unpack();
	iface = Gio.DBusNodeInfo.new_for_xml(xml).lookup_interface(iface);
	xml = iface.generate_xml(0).free(false);

	let ProxyClass = Gio.DBusProxy.makeProxyWrapper("<node>" + xml + "</node>");
	return new ProxyClass(conn, dest, path);
    };
    
    namespace.DBusChatServiceClient = new Lang.Class({
	Name: "DBusChatServiceClient",

	_init: function(nickname_in) {
	    let BusType = Gio.BusType.SYSTEM;
	    let Dest = "com.DBusChat.DBusChatService";
	    let DBusChatServicePath = "/com/DBusChat/DBusChatService";
	    let DBusChatServiceIface = "com.DBusChat.DBusChatService";
	    this.nickname = nickname_in;
	    this.client = new CreateDBusObject(BusType, Dest, DBusChatServicePath, DBusChatServiceIface);
	    this.client.connectSignal("NewMessage", this._new_message_cb);
	},

	_new_message_cb: function(Proxy, SenderName, params) {
	    let stdout = GLib.IOChannel.unix_new(1);
	    stdout.write_chars(params[0], -1);
	    stdout.flush();
	},

	_stdin_cb: function() {
	    let input = this.stdin.read_bytes(4096, null);
	    if(input.get_size() > 0) {
		this.client.SendMessageSync(this.nickname + ": " + input.get_data().toString());
		return true;
	    }
	    else {
		Mainloop.quit("default");
		return false;
	    }
	},

	listen_for_input: function() {
	    this.stdin = new Gio.UnixInputStream({"fd": 0, "close-fd": true});
	    let stdin_source = this.stdin.create_source(null);
	    stdin_source.set_callback(this._stdin_cb.bind(this));
	    stdin_source.attach(null);
	},
	
	shutdown_server: function() {
	    this.client.ShutdownSync();
	}
    });
}).call(null, DBusChat);

const main = function() {
    if(ARGV.length < 1) {
	print("[usage] " + System.programInvocationName + " stopserver|<nickname>");
	System.exit(1);
    }
    let client = new DBusChat.DBusChatServiceClient(ARGV[0]);
    if(ARGV[0] === "stopserver") {
	client.shutdown_server();
    }
    else {
	client.listen_for_input();
	Mainloop.run("default");
    }
}

main();
