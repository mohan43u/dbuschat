all: genexec

genexec: dbuschat-server.vala
	valac --pkg=gio-2.0 --pkg=posix dbuschat-server.vala

genc: dbuschat-server.vala
	valac -C --pkg=gio-2.0 --pkg=posix dbuschat-server.vala

install: all
	cp dbuschat-server /usr/bin/dbuschat-server
	cp dbuschat-client.js /usr/bin/dbuschat-client
	cp com.DBusChat.DBusChatService.conf /etc/dbus-1/system.d/
	cp com.DBusChat.DBusChatService.service /usr/share/dbus-1/system-services/

uninstall:
	rm -fr /usr/bin/dbuschat-server
	rm -fr /usr/bin/dbuschat-client
	rm -fr /etc/dbus-1/system.d/com.DBusChat.DBusChatService.conf
	rm -fr /usr/share/dbus-1/system-services/com.DBusChat.DBusChatService.service

clean:
	rm -fr dbuschat-server
	rm -fr dbuschat-server.c

.PHONY: clean uninstall
