namespace DBusChat {
	[DBus (name = "com.DBusChat.DBusChatService")]
	class DBusChatService : Object {
		private uint id;
		private MainLoop mainloop;
		public DBusChatService(MainLoop? mainloop_in = null) {
			this.mainloop = mainloop_in ?? new MainLoop();
			this.id = Bus.own_name(BusType.SYSTEM,
								   "com.DBusChat.DBusChatService",
								   BusNameOwnerFlags.NONE,
								   (connection) => {
									   try {
										   connection.register_object("/com/DBusChat/DBusChatService", this);
									   }
									   catch(Error e) {
										   stderr.printf("not able to register with DBus System Daemon\n");
										   this.shutdown();
									   }
								   },
								   () => {
									   stdout.printf("com.DBusChat.DBusChatService service started in DBus System Daemon\n");
								   },
								   () => {
									   stdout.printf("com.DBusChat.DBusChatService service stopped in DBus System Daemon\n");
									   this.shutdown();
								   });
		}
		public void run() {
			if(!this.mainloop.is_running()) this.mainloop.run();
		}
		public signal void new_message(string message);
		public void send_message(string message) {
			new_message(message);
		}
		public void shutdown() {
			if(this.id > 0) Bus.unown_name(this.id);
			if(this.mainloop.is_running()) {
				this.mainloop.quit();
			}
			else {
				Posix.exit(1);
			}
		}
	}
}

public int main(string[] args) {
	DBusChat.DBusChatService server = new DBusChat.DBusChatService();
	server.run();
	return 0;
}
