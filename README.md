### DBusChatService
A simple server-client using dbus. the server part is written in vala and the client part is written in gjs.

#### dbuschat-server
Server which is responsible for handling requests through com.DBusChat.DBusChatService DBus session service. It has three Following methods and signals,
* Method(s)
	* SendMessage - method to send some text to server
	* Shutdown - method to stop the server (only root user can call this method)
* Signal(s)
	* NewMessage - server will deliver incoming messages to all the subscribers of this signal

#### dbuschat-client
Client which will connect to server and send standard input. It also listens for any messages from server.

#### DBus Activation
com.DBusChat.DBusChatService service will be activated whenever a method invoked on this service.

#### Install
* sudo apt-get install valac gjs
* git clone https://github.com/mohan43u/dbuschat.git
* cd dbuschat
* sudo make install

#### Running
* dbuschat-client nickname

#### Uninstall
* cd dbuschat
* sudo make uninstall